﻿using System;
using System.Text;

namespace tiposdedados
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 1;
            Console.WriteLine(x);
            var y = 1.99;
            Console.WriteLine(y);
            System.Byte b = 8;
            byte b2 = 8;

            var valorDecimal = 1M;
            var valorLong = 1L;

            Console.WriteLine(int.MinValue);
            
            DateTime d;
            d = DateTime.Now;
            Console.WriteLine(d);
            DateTime d2 = d; //faz uma cópia dos dados

            StringBuilder s1 = new StringBuilder("teste");
            StringBuilder s2 = s1;
            Console.WriteLine(s1);
            s2.Append("X");
            Console.WriteLine(s1);

            //Tipos-valor anuláveis
            int? umvalor = null;
            Console.WriteLine(umvalor.HasValue);
            Console.WriteLine(umvalor);
            umvalor = 0;
            Console.WriteLine(umvalor.HasValue);
            Console.WriteLine(umvalor);
            umvalor = umvalor + 1;
            Console.WriteLine(umvalor);
            umvalor = null;
            //int outrovalor = umvalor.GetValueOrDefault(42);
            int outrovalor = umvalor ?? 42;
            Console.WriteLine(outrovalor);
        }
    }
}
